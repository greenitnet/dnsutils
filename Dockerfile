FROM debian:bullseye-slim
LABEL maintainer="tomasz.szymanski@greenit.com.pl"
RUN apt-get update \
  && apt-get install -yq \
     dnsutils \
     curl \
     libcap2-bin \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists

